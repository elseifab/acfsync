<?php
/*
Plugin Name: ACF Sync
Plugin URI: https://bitbucket.org/flowcom/acfsync
Description: The plugin is created to help teams holding control of versions of Advanced Custom Fields Group Definitions over developer machines.
Author: Andreas Ek
Version: 0.1
Author URI: http://www.flowcom.se
*/

include_once 'helpers/tabs.php';
include_once 'admin/settings.php';
include_once 'acf/update.php';


function acfsync_plugin_url() {
	return plugins_url( '', __FILE__ );
}

function acfsync_plugin_dir() {
	$path = plugin_dir_path( __FILE__ );
	return rtrim( $path, '/' );
}
