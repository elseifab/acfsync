<?php

namespace flowcom\acfsync\helpers;

class Tabs {

	public $items;

	function __construct() {

		$prefix = str_replace( '\\', '_', get_class() ) . '_';

		$this->items = apply_filters( $prefix . 'item', array() );

		if ( sizeof( $this->items ) ) {

			$keys        = array_keys( $this->items );
			$current_tab = $keys[0];

			if ( isset( $_REQUEST['tab'] ) && !empty( $_REQUEST['tab'] ) ) {
				$current_tab = $_REQUEST['tab'];
			}

			if ( wp_verify_nonce( $_REQUEST['nonce'], 'update_tab' ) ) {
				echo "<br/>";
				do_action( $prefix . 'update_' . $current_tab );
			}

			echo '<h2 class="nav-tab-wrapper">';
			foreach ( $this->items as $tab => $name ) {
				$class = ( $tab == $current_tab ) ? ' nav-tab-active' : '';
				echo "<a class=\"nav-tab$class\" href=\"?page=$_REQUEST[page]&tab=$tab\">$name</a>";
			}
			echo '</h2>';

			?>
			<form method="post">

				<?php do_action( $prefix . 'display_' . $current_tab ); ?>

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'newsflow' ) ?>" />
					<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'update_tab' ); ?>" />
					<input type="hidden" name="tab" value="<?php echo $current_tab; ?>" />
				</p>
			</form>
		<?php


		}

	}


}
