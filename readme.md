# ACF Sync #

## Status
This WordPress / ACF plugin is in experimental phase.

## Purpose
The plugin is created to help teams holding control of versions of Advanced Custom Fields Group Definitions over developer machines.

The Field Groups are saved into configurated path per field group.

Supported to ACF 5 only.

## How to get started

1. Install the plugin and activate.
1. Go to Tools and the new menu "ACF Sync".
1. Set your **path** to each ACF Group Field listed where the data files will be created/updated.
1. Your settings will be saved into the file **settings.json** in this plugin folder to make it possible to version control the common team settings. Make sure the plugin root is writeable!

**Now, when updating ACF, two files per group will be created/updated:**

**{key}.json** with the group data for backup as import file.

**{key}.php** as possible include to test and production environment.

Now it's possible for your team to version control the ACF definitions.

Good luck! Please, leave feedback!

## Recommendations
Please, do not activate this plugin in a test or production environment. It is only to use in local version controlled development environment.

Fork and contribute via pull requests to make your improvements!

## Troubleshooting
No files generated, please take a look at the defined path/folder permissions!

## Next step

* Version control and warnings (not for sure)
* General base class template code generated for views and other helpers

## Contact

Developer: Andreas Ek, twitter @ekandreas