<?php

namespace flowcom\acfsync\admin;

include_once 'settings-path.php';

class Settings {

	function __construct() {
		add_action( 'admin_menu', array( &$this, 'menues' ) );
	}

	function menues() {
		$handle = add_submenu_page( 'tools.php', 'ACF Sync', 'ACF Sync', 'manage_options', 'acfsync-settings', array( 'flowcom\\acfsync\\admin\\Settings', 'display' ) );
	}

	function display() {

		echo '<div id="icon-plugins" class="icon32"></div>';
		echo '<h2><img src="' . acfsync_plugin_url() . '/assets/img/newsflow_gray_150.png" width="32" /> ACF Sync</h2>';
		echo '<div class="description">This plugin synchronize ACF-settings between developers. Advanced Custom Fields v5 supported.</div>';

		$tabs = new \flowcom\acfsync\helpers\Tabs();

	}

}

new Settings();

