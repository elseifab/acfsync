<?php

namespace flowcom\acfsync\admin\settings;

use flowcom\acfsync\acf\Update;

class Path {

	public $settings;

	function __construct() {
		add_filter( 'flowcom_acfsync_helpers_Tabs_item', array( &$this, 'item' ), 10 );
		add_action( 'flowcom_acfsync_helpers_Tabs_display_path', array( &$this, 'display' ) );
		add_action( 'flowcom_acfsync_helpers_Tabs_update_path', array( &$this, 'update' ) );
	}

	function item( $items ) {
		$items['path'] = __( 'Path', 'acfsync' );
		return $items;
	}

	function display() {

		//TODO: Save settings file into configurable path in the settings path tab, possible plugin update later on...

		$settings       = file_get_contents( acfsync_plugin_dir() . '/settings.json' );
		$this->settings = json_decode( $settings, TRUE );
		$path           = $this->settings['path'];
		$field_groups   = acf_get_field_groups();

		if ( !sizeof( $field_groups ) ) {
			?>
			<p>
				There is no Field Groups defined in ACF to map for output folder path. Please, create groups and then come back here :-)
			</p>
			<?php
			return;
		}

		?>
		<p>
			This tab defines the path to save the field definition as files when updating the ACF-structures.<br />
			The values will be stored inside a file and therefore version tracked to the other developers in your team.
		</p>
		<?php

		foreach ( $field_groups as $group ) {
			?>
			<p>
				<label for="beta"><strong><?php echo $group['title']; ?></strong></label><br />
				<input name="group_key[]" value="<?php echo $group['key']; ?>" type="hidden" />
				<input name="group_path[]" value="<?php echo $path[$group['key']]; ?>" type="text" id="url" class="large-text" />
				<span class="description">Path relative to web root, eg /wp-content/plugins/acfsync/conf</span>
			</p>
		<?php
		}

		wp_nonce_field( 'acfsync-update', 'path-tab' );

	}

	function update() {

		if ( !empty( $_POST ) && check_admin_referer( 'acfsync-update', 'path-tab' ) ) {

			$updater = new Update();

			$path = array();
			foreach ( $_REQUEST['group_key'] as $key => $value ) {
				$path[$value] = $_REQUEST['group_path'][$key];
			}
			$this->settings['path'] = $path;

			$settings = json_encode( $this->settings );
			file_put_contents( acfsync_plugin_dir() . '/settings.json', $settings );

			$field_groups   = acf_get_field_groups();

			if ( sizeof( $field_groups ) ) {
				foreach ( $field_groups as $group ) {
					$updater->field_group( $group );
				}
			}

			?>
			<div class="updated">
				Start settings saved.
			</div>
		<?php
		}

	}

}

new Path();