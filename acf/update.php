<?php

namespace flowcom\acfsync\acf;

class Update {

	function __construct() {
		add_action( 'acf/update_field_group', array( &$this, 'field_group' ), 5, 1 );
	}

	function field_group( $field_group ) {

		$settings = file_get_contents( acfsync_plugin_dir() . '/settings.json' );
		$settings = json_decode( $settings, TRUE );
		$path     = $settings['path'];

		$key = $field_group['key'];

		if ( isset( $path[$key] ) ) {

			$path = get_home_path() . untrailingslashit( $path[$key] );

			if ( is_writable( $path ) ) {
				add_filter( 'acf/settings/save_json', function () use ( $path ) {
							return $path;
						} );

				$fields                = acf_get_fields( $field_group );
				$fields                = acf_prepare_fields_for_export( $fields );
				$field_group['fields'] = $fields;
				$id                    = acf_extract_var( $field_group, 'ID' );

				$php = "<?php\r\n// Generated via ACF Sync plugin, Digital United.\r\n\r\n";
				$php .= "if( function_exists('register_field_group') ):" . "\r\n" . "\r\n";

				$code = var_export( $field_group, true );
				$code = str_replace( "  ", "\t", $code );
				$code = preg_replace( '/([\t\r\n]+?)array/', 'array', $code );
				$code = preg_replace( '/[0-9]+ => array/', 'array', $code );

				$php .= "register_field_group({$code});" . "\r\n" . "\r\n";
				$php .= "endif;";

				file_put_contents( $path . '/' . $field_group['key'] . '.php', $php );
			}

		}

	}

}

new Update();